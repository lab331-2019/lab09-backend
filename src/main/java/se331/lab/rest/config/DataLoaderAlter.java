package se331.lab.rest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import se331.lab.rest.entity.Student;
import se331.lab.rest.repository.StudentRepository;

@Component
public class DataLoaderAlter implements ApplicationRunner {

    @Autowired
    StudentRepository studentRepository;
    @Override
    public void run(ApplicationArguments args) throws Exception {
        studentRepository.save(Student.builder()
                .id(66L)
                .studentId("SE-666")
                .name("Baba")
                .surname("Yaga")
                .gpa(3.96)
                .image("https://vignette.wikia.nocookie.net/john-wick8561/images/3/3e/John-wick-chapter-2.jpeg/revision/latest?cb=20190826173857")
                .penAmount(66)
                .description("He has a city to burn")
                .build());

    }

}
