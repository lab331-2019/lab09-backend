package se331.lab.rest.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.StudentDao;
import se331.lab.rest.dao.StudentDaoAltImpl;
import se331.lab.rest.dao.StudentDaoImpl;
import se331.lab.rest.entity.Student;

import java.util.List;
@Slf4j
@Service
public class StudentServiceImpl implements StudentService{

    //StudentDao studentDao = new StudentDaoAltImpl();
    @Autowired
    StudentDao studentDao /*= new StudentDaoImpl()*/;
    @Override
    public List<Student> getAllStudent() {
        log.info("service received called");
        List<Student> students = this.studentDao.getAllStudent();
        log.info("service received {} \n from dao",students);
        return students;
    }

    @Override
    public Student findById(Long id) {
        return studentDao.findById(id);
    }

    @Override
    public Student saveStudent(Student student) {
        return studentDao.saveStudent(student);
    }
}

