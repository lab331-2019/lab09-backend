package se331.lab.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;
@Slf4j
@Repository
@Profile("MeinDao")
public class StudentDaoAltImpl implements StudentDao{
    List<Student> students;
    public StudentDaoAltImpl(){
        this.students = new ArrayList<>();
        this.students.add(Student.builder()
                .id(66L)
                .studentId("SE-666")
                .name("Baba")
                .surname("Yaga")
                .gpa(3.96)
                .image("https://vignette.wikia.nocookie.net/john-wick8561/images/3/3e/John-wick-chapter-2.jpeg/revision/latest?cb=20190826173857")
                .penAmount(66)
                .description("He has a city to burn")
                .build());
    }

    @Override
    public List<Student> getAllStudent() {

        log.info("Mein dao is called");
        return students;
    }

    @Override
    public Student findById(Long id) {
        return students.get((int) (id -1));
    }

    @Override
    public Student saveStudent(Student student) {
        student.setId((long) students.size());
        students.add(student);
        return student;
    }
}
